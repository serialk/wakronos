This program has been created for lazy EPITA students (and works therefore for
other IONIS students). It retrieves each day the hour of the first class and
sets the alarm automatically.

# Installation

## Packages

You first need these dependancies : `python3 python3-tk mplayer pip3`

Then you can install the python modules :
    
    # pip3 install tkinter mplayer.py requests yaml

# Usage

Just launch `wakronos.py`. You have a lot of ways to let it run in background :

    :::console
    $ nohup ./wakronos.py &
    $ screen -S wakronos -d -m ./wakronos.py
    $ …

You can also create a daemon that launches the script as your user.

When the alarm triggers, a window appears and you have two options :

* **Ta gueule**: stop the alarm and close the window.
* **Snooze**: pause the alarm and wait for a little delay before replaying it.

# Configuration

Edit the `conf.yml` file and you can custom these options :

## ichronos

    :::yaml
    url: "http://ichronos.in/?s={group}&w={week}&api" # API URL, don't touch.
    check-hour: 1 # first class hour is checked at 1 AM
    group: "INFOSPEB2" # your group on chronos

## alarm

    :::yaml
    sound: "/home/antoine/reveil.mp3" # the path to your music
    delay: 30 # How many minutes do you need from your bed to EPITA?
    snooze-delay: 5 # delay when "snooze" is pressed in minutes

## blacklist

    :::yaml
    - Big-data innovation
    - Green IT
    - Cloud computing
    - Add other ininteresting classes here

