#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# "THE BEER-WARE LICENSE" (Revision 42):
# <antoine.pietri@epita.fr> wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer in return. Antoine Pietri

import requests
import yaml
import subprocess
import datetime
import time
import os.path
import sys
import tkinter as tk
import mplayer
import threading

if len(sys.argv) > 1:
    confpath = sys.argv[1]
else:
    confpath = os.path.join(os.path.dirname(__file__), 'conf.yml')
CONF = yaml.load(open(confpath))

def today():
    return datetime.date.today().strftime('%d/%m/%Y')

def ichronos(group, week=None):
    week = str(week) if week else ''
    url = CONF['ichronos']['url'].format(group=group, week=week)
    try:
        response = requests.get(url)
        return response.json
    except:
        return None

def today_classes(classes, blacklist=[]):
    if not classes:
        return None
    classes = filter(lambda x: x[0] == today(), classes) # only today
    classes = filter(lambda x: x[4] not in blacklist, classes) # blacklist
    return list(classes)

def first_class(classes):
    def strphour(s):
        h, m = map(int, s.split('h'))
        return h * 60 + m
    if not classes:
        return None
    classes.sort(key=lambda x: strphour(x[1]))
    return strphour(classes[0][1]), classes[0][7] # hour, place

def alert(place):
    p = mplayer.Player()
    delay = CONF['alarm']['snooze-delay']
    def play():
        p.loadfile(CONF['alarm']['sound'])
    play()
    t = threading.Timer(delay * 60, play)
    def snooze():
        p.stop()
        t.start()
    root = tk.Tk()
    def delwindow():
        p.quit()
        try:
            t.cancel()
        except:
            pass
        root.destroy()
    root.protocol("WM_DELETE_WINDOW", delwindow)
    root.title('wakronos')
    w = tk.Label(root, text='Cours en {place}'.format(place=place))
    bquit = tk.Button(root, text='Ta gueule', command=root.destroy)
    bsnooze = tk.Button(root, text='Snooze', command=snooze)
    w.pack()
    bquit.pack()
    bsnooze.pack()
   
def main():
    last_check = '0/0/0'
    alarm = None
    today_done = False
    place = '???'
    while True:
        n = datetime.datetime.now()
        nt = n.hour * 60 + n.minute
        if CONF['ichronos']['check-hour'] <= n.hour and last_check != today():
            classes = ichronos(CONF['ichronos']['group'])
            t, place = first_class(today_classes(classes, CONF['blacklist']))
            if t:
                alarm = t - CONF['alarm']['delay']
                last_check = today()
                if nt <= alarm:
                    print('{}: alarm set for {}:{:02d}'.format(
                        last_check, alarm // 60, alarm % 60))
                    today_done = False
                else:
                    today_done = True

        if (alarm and last_check == today() and
           alarm <= nt) and not today_done:
            print('{}:{:02d}: alarm triggered.'.format(nt//60, nt%60))
            alert(place)
            alarm = None
            today_done = True
        time.sleep(10)

if __name__ == '__main__':
    main()
